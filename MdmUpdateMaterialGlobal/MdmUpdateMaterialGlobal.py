import base64
import boto3
import json
import logging
import requests
import os 
import sqlalchemy as db
from datetime import datetime
# from FieldMap import B2ERP, SAP2Tag, B2RS, RS2TBL
from sqlalchemy import update, and_, select, func, text
from sqlalchemy.exc import SQLAlchemyError
# from SAPMaterialDM import materialUpdate
from botocore.exceptions import ClientError
from itertools import zip_longest

# Set up Logging feature for CloudWatch Logs
Lambda_logger = logging.getLogger()
Lambda_logger.setLevel(logging.INFO)

# Set up Boto3 clients
lambda_client = boto3.client('lambda')

# Retrieve the environment variables 
secret_name = os.environ['PIPO_SECRET']
region_name = os.environ['Region']

MDM_MM_SCHEMA = os.environ['MDM_MM_SCHEMA']
MDM_MM_MATERIAL = os.environ["MDM_MM_MATERIAL"]
MDM_MM_PLANT = os.environ["MDM_MM_PLANT"]
MDM_MM_SALES = os.environ["MDM_MM_SALES"]
MDM_MM_CLASS = os.environ["MDM_MM_CLASS"]
MDM_MM_GOLDEN = os.environ['MDM_MM_GOLDEN']

MDM_MM_PH_SCHEMA = os.environ['MDM_MM_PH_SCHEMA']
MDM_MM_PH = os.environ['MDM_MM_PH']
# Get the RedShift related secret keys from AWS Secret Manager
secret_name = os.environ['RS_SECRET']
region_name = os.environ['Region']


# Flags for JSONFieldMapping module (constants)
erp = 1
business = 2
mdmdw = 3
rs2bus = 4

# Initialize global variables
wfData = {}
response = {}
secrets = {}

def lambda_handler(event, context):

    # global lambda_client
    # cfg = botocore.config.Config(retries={'max_attempts': 0}, read_timeout=LAMBDA_READ_TIMEOUT)
    # lambda_client = boto3.client('lambda', config=cfg)

    Lambda_logger.info('Starting Update Global Material Process')

    # Get Incoming data
    message = event['Message'].copy()
    #print ('Message :: ', message)
    job_secrets = get_secret(secret_name, region_name)
    secrets = json.loads(job_secrets)
    if secrets: 
        RS_CONN_STR = 'postgresql://'+secrets['username']+':'+secrets['password']+'@'
        RS_CONN_STR += secrets['host']+':'+str(secrets['port'])+'/'+secrets['database']
        # print ('Connection :: ', RS_CONN_STR)
        Lambda_logger.info('Configured User :: %s', secrets['username'])

    else: 
        return {'STATUS_CODE':-1, 'STATUS_MSG':'Unable to Retrieve Secrets'}
    
    engine = db.create_engine(RS_CONN_STR)

    try:
        #engine = db.create_engine(RS_CONN_STR)
        Lambda_logger.info('Connecting RedShift DB')
        connection = engine.connect()
        md = db.MetaData()

        # Get Material object 
        material1 = db.Table(MDM_MM_MATERIAL, md, autoload  = True, autoload_with = engine, schema = MDM_MM_SCHEMA)
        material1_golden = db.Table(MDM_MM_GOLDEN, md, autoload  = True, autoload_with = engine, schema = MDM_MM_SCHEMA)
        material1_plant = db.Table(MDM_MM_PLANT, md, autoload  = True, autoload_with = engine, schema = MDM_MM_SCHEMA)
        material1_sales = db.Table(MDM_MM_SALES, md, autoload  = True, autoload_with = engine, schema = MDM_MM_SCHEMA)
        glb_ph = db.Table(MDM_MM_PH, md, autoload  = True, autoload_with = engine, schema = MDM_MM_PH_SCHEMA)
        Lambda_logger.info('DB Connected')

    except SQLAlchemyError as e:
        global response
        response.update({'STATUS_CODE': -1})
        response.update({'STATUS_MSG':'RedShift Connection Error'})
        Lambda_logger.info('RedShift Connection Issue :: %s', e)
        return response

    Lambda_logger.info('Analysing incoming message.... Total messages received : %s', len(message['Data']))

    for data in message['Data']:

        # Pull the respective SystemName for this material
        # qSystem = 'SELECT source from ' + MDM_MM_SCHEMA + '.' + MDM_MM_MATERIAL + ' where material_number = ' + "'" + tempMaterial + "'"
        # result = connection.execute(qSystem)
        # tempSystem = result.fetchall()

        basicFields = {}
        basicFields['SystemName'] = message['SystemName']
        basicFields['WorkflowId'] = message['WorkflowId']
        basicFields['OperationName'] = message['OperationName']
        basicFields['TaskId'] = message['TaskId']

        # Form message for redshift update
        dataSet = {}
        dataSet['SystemName'] = data['SystemName']
        dataSet['MdmNumber'] = data['MdmNumber']
        dataSet['MaterialNum'] = data['MaterialNum']
        dataSet['MaterialGroup'] = data['MaterialGroup']
        dataSet['PH1Desc'] = message['Data'][0]['PH1Desc']
        dataSet['PH2Desc'] = message['Data'][0]['PH2Desc']
        dataSet['PH3Desc'] = message['Data'][0]['PH3Desc']
        dataSet['PH4Desc'] = message['Data'][0]['PH4Desc']
        dataSet['PH5Desc'] = message['Data'][0]['PH5Desc']
        dataSet['PH6Desc'] = message['Data'][0]['PH6Desc']
        dataSet['PH7Desc'] = message['Data'][0]['PH7Desc']
        dataSet['LifecycleDesc'] = message['Data'][0]['LifecycleDesc']
        dataSet['LifecycleStatus'] = message['Data'][0]['LifecycleStatus']
        dataSet['GoldenRecordIndicator'] = message['Data'][0]['GoldenRecordIndicator']

        basicFields['Data'] = dataSet

        Lambda_logger.info("Message formed for redshift update :: %s", basicFields)
        response = ProcessMaterials(basicFields, engine , material1_golden, material1, material1_plant, material1_sales, connection, glb_ph)
        
    return response

def ProcessMaterials(basicFields, engine, material1_golden, material1, material1_plant, material1_sales, connection, glb_ph):

    # Setup basic Workflow Details
    wfData = dict()
    wfData['WorkflowId'] = basicFields['WorkflowId']
    wfData['OperationName'] = basicFields['OperationName']
    wfData['TaskId'] = basicFields['TaskId']
    wfData['SystemName'] = basicFields['Data']['SystemName']
    wfData['MdmNumber'] = basicFields['Data']['MdmNumber']
    wfData['MaterialNum'] = basicFields['Data']['MaterialNum']

    mdmData = MapJsonFields(basicFields, mdmdw)
    #Lambda_logger.info('RedShift Input mdmdata :: %s', mdmData)
    mdmData = json.loads(mdmData)

    # Update the MDM material data 
    dwresp = UpdateMDMMaterial(mdmData, engine, material1_golden, material1, material1_plant, material1_sales, connection, glb_ph, basicFields)

    #dwresp = {'STATUS_CODE':200}
    
    # Check if the MDM material is updated
    if dwresp['STATUS_CODE'] == 200:
        Lambda_logger.info('Udpated Global - Success')
        wfData['IsSuccess'] = "TRUE"
        response.update({'STATUS_CODE':200})
        response.update({'STATUS_MSG':'Udpated Global - Success'})
    else:
        Lambda_logger.info('Udpated Global - Failure')
        wfData['IsSuccess'] = "FALSE"
        response.update({'STATUS_CODE':-1})
        response.update({'STATUS_MSG':'Udpated Global - Failure'})

    Lambda_logger.info('Success : Update global material - Process Ended')
    
    # Publish the WF message
    print ('Workflow Message : ', wfData)
    wfResponse = publishWF(json.dumps(wfData))
    print ('Response to Process : ', wfResponse)
    return response

def MapJsonFields(businessFields, convType):
    MDM_FIELD_MAPPING = os.environ['FN_FIELD_MAP']
    businessFields['Flag'] = convType

    response = lambda_client.invoke(
        FunctionName=MDM_FIELD_MAPPING,
        InvocationType='RequestResponse',
        Payload=json.dumps(businessFields)
        )  
    record = response['Payload'].read().decode('utf8').replace("'", '"')
    Lambda_logger.info('Response from Mapping fields :: %s', record)
    return json.loads(record)

def UpdateMDMMaterial(mdmData, engine, material1_global, material1, material1_plant, material1_sales,connection, glb_ph, basicFields):
    # Read environment variables 
    mand_keys = mdmData[MDM_MM_GOLDEN]['keys']
    print("Mandatory Keys :", mand_keys)
    Lambda_logger.info("Data to be pushed into Redshift tables :: %s", mdmData)
    Lambda_logger.info("Table Keys :: %s",mdmData.keys())
    upquery = ""
    
    for table in mdmData.keys():
        
        keys = mdmData[table]['keys']
        # keys.update(mand_keys)
        # print("Updated Keys :", keys)
        values =  mdmData[table]['values']
        if(values!={}):
            Lambda_logger.info('Table in Progress :: %s', table)

            if table == MDM_MM_MATERIAL: 
                Lambda_logger.info('Updating Material Table')
                upquery = update(material1).where(
                    and_(
                        material1.c.source == mand_keys['source_system'],
                        material1.c.globalmdm == mand_keys['globalmdm'],
                        material1.c.material_number == mand_keys['material_number']
                        # material1_plant.c.material_group == keys['material_group']
                    )
                )
            elif table == MDM_MM_PLANT:
                if keys['plant'] != "":  
                    Lambda_logger.info('Updating Material Plant Table')            
                    upquery = update(material1_plant).where(
                        and_(
                            material1_plant.c.source == mand_keys['source_system'],
                            material1_plant.c.globalmdm == mand_keys['globalmdm'],
                            material1_plant.c.material_number == mand_keys['material_number'],
                            material1_plant.c.plant == keys['plant']
                            # material1_plant.c.profit_center == keys['profit_center']
                        )
                    )
            elif table == MDM_MM_SALES:
                if keys['sales_org'] != "":
                    Lambda_logger.info('Updating Material Sales Table')
                    upquery = update(material1_sales).where(
                        and_(
                            material1_sales.c.source == mand_keys['source_system'],
                            material1_sales.c.globalmdm == mand_keys['globalmdm'],
                            material1_sales.c.material_number == mand_keys['material_number'],
                            material1_sales.c.distr_channel==keys['distr_channel'],
                            material1_sales.c.sales_org == keys['sales_org']
                            # material1_sales.c.material_pricing_grp == keys['material_pricing_grp']              
                        )
                    )
            elif table == MDM_MM_GOLDEN:
                Lambda_logger.info('Updating Material Golden Table')
                upquery = update(material1_global).where(
                    and_(
                        material1_global.c.source_system == mand_keys['source_system'],
                        material1_global.c.globalmdm == mand_keys['globalmdm'],
                        material1_global.c.material_number == mand_keys['material_number']
                    )
                )
            elif table == MDM_MM_PH:
                # Query the table for presence of this material
                temp = select([func.count()]).select_from(glb_ph).where(
                    and_(
                        glb_ph.c.source_id == mand_keys['source_system'],
                        glb_ph.c.material == mand_keys['material_number']
                    )
                )
                tempResponse = connection.execute(temp)
                tblCnt = tempResponse.first()[0]
                print("No Of Rows in PH Table : ", tblCnt)

                if tblCnt == 0:

                    Lambda_logger.info('Inserting record into Global PH Table')

                    phRecordSet = {}
                    dateTimeObj = datetime.now()

                    phRecordSet['source_id'] = mand_keys['source_system']
                    phRecordSet['material'] = mand_keys['material_number']
                    phRecordSet['material_desc'] = "null" 
                    phRecordSet['material_type'] = "null"
                    phRecordSet['ph1'] = "null"
                    phRecordSet['ph2'] = "null"
                    phRecordSet['ph3'] = "null" 
                    phRecordSet['ph4'] = "null"
                    phRecordSet['ph5'] = "null"
                    phRecordSet['ph6'] = "null"
                    phRecordSet['ph7'] = "null"
                    phRecordSet['ph1_desc'] = basicFields['Data']['PH1Desc']
                    phRecordSet['ph2_desc'] = basicFields['Data']['PH2Desc']
                    phRecordSet['ph3_desc'] = basicFields['Data']['PH3Desc']
                    phRecordSet['ph4_desc'] = basicFields['Data']['PH4Desc']
                    phRecordSet['ph5_desc'] = basicFields['Data']['PH5Desc']
                    phRecordSet['ph6_desc'] = basicFields['Data']['PH6Desc']
                    phRecordSet['ph7_desc'] = basicFields['Data']['PH7Desc']
                    phRecordSet['fromdate'] = dateTimeObj
                    phRecordSet['todate'] = dateTimeObj
                    
                    print("PH Table Input :: ", phRecordSet)
                    temp = " BEGIN; LOCK "+MDM_MM_PH_SCHEMA+"."+MDM_MM_PH + "; "
                    temp += """ insert into  ["""+MDM_MM_PH_SCHEMA+"""].["""+MDM_MM_PH+"""] values ("""
                    for val in phRecordSet:
                        temp += ":{}, ".format(val)
                        temp2 = temp.rstrip(", ")
                        temp2 += "); "
                        temp2 += "END;"  

                    try:
                        connection.execution_options(autocommit=True).execute(text(temp2), **phRecordSet)
                        Lambda_logger.info('Successfully backed up the material details to PH tables as new Row item')
                        connection.close()

                    except SQLAlchemyError as e:
                        if 'connection' in locals():
                            connection.close()
                        if 'engine' in locals():
                            engine.dispose()
                            Lambda_logger.info('Engine disposed')
                        Lambda_logger.info('Unable to Create MDM Master Record :: %s', e)
                    
                    continue 

                    
                else:
                    Lambda_logger.info('Updating Global PH Table')
                    upquery = update(glb_ph).where(
                        and_(
                            glb_ph.c.source_id == mand_keys['source_system'],
                            glb_ph.c.material == mand_keys['material_number']
                        )
                    )

            try: 
                if upquery != "":
                    upquery = upquery.values(**values)
                    uptable = upquery.table
                    lockquery = "LOCK " + str(uptable) + "; "
                    try:        
                        Lambda_logger.info("Formed Query is :: %s", upquery)
                        trans = connection.begin()
                        _lockResponse = connection.execution_options(autocommit=True).execute(lockquery)
                        _aggResponse = connection.execution_options(autocommit=True).execute(upquery)
                        trans.commit()

                        Lambda_logger.info('Record Successfully Updated for the table : %s', uptable)
                        response.update({'STATUS_CODE': 200})
                        response.update({'STATUS_MSG': 'Redshift update successful'}) 

                    except SQLAlchemyError as e:
                        Lambda_logger.info('Unable to Update redshift tables :: %s', e) 
                        response.update({'STATUS_CODE': -1})
                        response.update({'STATUS_MSG': 'Unable to Update redshift tables'})
                          
                else:
                    Lambda_logger.info('Table : %s was skipped due to null values', uptable)
                    response.update({'STATUS_CODE': -1})
                    response.update({'STATUS_MSG': 'Update Query is empty'})
                

            except SQLAlchemyError as e:
                Lambda_logger.info('Unable to Update Record :: %s', e)
                response.update({'STATUS_CODE': -1})
                response.update({'STATUS_MSG': 'Unable to Update Record'})
              
    return response
 
def UpdateErpMaterial(data):
    Lambda_logger.info('Stage:ERP - Starting the process "Update Material Global"')
    
    # Assign environment variables 
    secret_name = os.environ['PIPO_SECRET']
    region_name = os.environ['Region']

    # Get the RedShift related secret keys from AWS Secret Manager
    job_secrets = get_secret(secret_name, region_name)
    secrets = json.loads(job_secrets)
    if secrets: 
        PIPO_USER = secrets['username']
        PIPO_PWD = secrets['password']
        PIPO_AUTH = secrets["auth"]
    else: 
        return {'STATUS_CODE':-1, 'STATUS_MSG':'Unable to Get Secrets'}

    PIPO_CUST_UPDT = os.environ['PIPO_ENDPOINT_UPDATE']
    # Setup Header and trgger PIPO endpoint
    headers = {"Authorization":PIPO_AUTH}
    payload = json.loads(data)
    response = requests.post(PIPO_CUST_UPDT,data=payload, timeout=30, auth=(PIPO_USER,PIPO_PWD), headers=headers)
    #print("response is",response)
    res = response.json()
    return res['E_RETURN']

def publishES(data):
    # This method will publish MDM record to Elastic Search SNS Topic
    MDM_ES_SNS = secrets['PUBLISH_ES_SNS']
    response = lambda_client.invoke(
        FunctionName=MDM_ES_SNS,
        InvocationType='RequestResponse',
        Payload=json.dumps(data)
        )
    record = response['Payload'].read().decode('utf8').replace("'", '"')
    return json.loads(record)    
    
def publishWF(data):
    # This method will publish upstream response
    MDM_WF_SNS = os.environ['PUBLISH_WF_SNS']
    response = lambda_client.invoke(
        FunctionName=MDM_WF_SNS,
        InvocationType='RequestResponse',
        Payload=json.dumps(data)
        )
    record = response['Payload'].read().decode('utf8').replace("'", '"')
    return json.loads(record)

def get_secret(secret_name, region_name):
    
    # Create a Secrets Manager client
    Lambda_logger.info('Reading Secrets from AWS')    
    client = boto3.client(
        service_name='secretsmanager',
        region_name=region_name
    )

    try:
        get_secret_value_response = client.get_secret_value(
            SecretId=secret_name
        )
    except ClientError as e:
        if e.response['Error']['Code'] == 'DecryptionFailureException':
            # Secrets Manager can't decrypt the protected secret text using the provided KMS key.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'InternalServiceErrorException':
            # An error occurred on the server side.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'InvalidParameterException':
            # You provided an invalid value for a parameter.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'InvalidRequestException':
            # You provided a parameter value that is not valid for the current state of the resource.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'ResourceNotFoundException':
            # We can't find the resource that you asked for.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
    else:
        # Decrypts secret using the associated KMS CMK.
        # Depending on whether the secret is a string or binary, one of these fields will be populated.
        if 'SecretString' in get_secret_value_response:
            secret = get_secret_value_response['SecretString']
            Lambda_logger.info('Secret has been retrieved')
            Lambda_logger.info('Secrets :: %s', secret_name)
            return secret
        else:
            decoded_binary_secret = base64.b64decode(get_secret_value_response['SecretBinary'])
            return decoded_binary_secret