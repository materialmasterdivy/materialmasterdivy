import base64
import boto3
import json
import logging
import requests
import os 
import sqlalchemy as db
from FieldMap import B2ERP, SAP2Tag, B2RS, RS2TBL
from sqlalchemy import update, and_, select
from sqlalchemy.exc import SQLAlchemyError
from SAPMaterialDM import materialUpdate
from botocore.exceptions import ClientError
from itertools import zip_longest

# Set up Logging feature for CloudWatch Logs
Lambda_logger = logging.getLogger()
Lambda_logger.setLevel(logging.INFO)

# Set up Boto3 clients
lambda_client = boto3.client('lambda')

# Retrieve the environment variables 
secret_name = os.environ['PIPO_SECRET']
region_name = os.environ['Region']

MDM_MM_SCHEMA = os.environ['MDM_MM_SCHEMA']
MDM_MM_MATERIAL = os.environ["MDM_MM_MATERIAL"]
MDM_MM_PLANT = os.environ["MDM_MM_PLANT"]
MDM_MM_SALES = os.environ["MDM_MM_SALES"]
MDM_MM_CLASS = os.environ["MDM_MM_CLASS"]
MDM_MM_GOLDEN = os.environ['MDM_MM_GOLDEN']

# Get the RedShift related secret keys from AWS Secret Manager
secret_name = os.environ['RS_SECRET']
region_name = os.environ['Region']


# Flags for JSONFieldMapping module (constants)
erp = 1
business = 2
mdmdw = 3
rs2bus = 4

# Initialize global variables
wfData = {}
response = {}
secrets = {}

def lambda_handler(event, context):

    # global lambda_client
    # cfg = botocore.config.Config(retries={'max_attempts': 0}, read_timeout=LAMBDA_READ_TIMEOUT)
    # lambda_client = boto3.client('lambda', config=cfg)

    Lambda_logger.info('--------------------------------------------------------------------------------')
    Lambda_logger.info('Starting Update Global Material Process')

    # Get Incoming data
    message = event['Message'].copy()
    #print ('Message :: ', message)
    job_secrets = get_secret(secret_name, region_name)
    secrets = json.loads(job_secrets)
    if secrets: 
        RS_CONN_STR = 'postgresql://'+secrets['username']+':'+secrets['password']+'@'
        RS_CONN_STR += secrets['host']+':'+str(secrets['port'])+'/'+secrets['database']
        # print ('Connection :: ', RS_CONN_STR)
        Lambda_logger.info('Configured User :: %s', secrets['username'])

    else: 
        return {'STATUS_CODE':-1, 'STATUS_MSG':'Unable to Retrieve Secrets'}
    
    engine = db.create_engine(RS_CONN_STR)

    try:
        #engine = db.create_engine(RS_CONN_STR)
        Lambda_logger.info('Connecting RedShift DB')
        connection = engine.connect()
        md = db.MetaData()

        # Get Material object 
        material1 = db.Table(MDM_MM_MATERIAL, md, autoload  = True, autoload_with = engine, schema = MDM_MM_SCHEMA)
        material1_golden = db.Table(MDM_MM_GOLDEN, md, autoload  = True, autoload_with = engine, schema = MDM_MM_SCHEMA)
        material1_plant = db.Table(MDM_MM_PLANT, md, autoload  = True, autoload_with = engine, schema = MDM_MM_SCHEMA)
        material1_sales = db.Table(MDM_MM_SALES, md, autoload  = True, autoload_with = engine, schema = MDM_MM_SCHEMA)
        Lambda_logger.info('DB Connected')

    except SQLAlchemyError as e:
        global response
        response.update({'STATUS_CODE': -1})
        response.update({'STATUS_MSG':'RedShift Connection Error'})
        Lambda_logger.info('RedShift Connection Issue :: %s', e)
        return response

    Lambda_logger.info('Analysing incoming message.... Total messages received : %s', len(message['Data']))
    Lambda_logger.info('--------------------------------------------------------------------------------')
    i = 1
    sysName = ""
    # erpDataBlock = []

    for data in message['Data']:
        
        Lambda_logger.info('Processing Message # %s', i)
        tempMaterial = data['MaterialNum']

        # Pull the respective SystemName for this material

        qSystem = 'SELECT source from ' + MDM_MM_SCHEMA + '.' + MDM_MM_MATERIAL + ' where material_number = ' + "'" + tempMaterial + "'"
        result = connection.execute(qSystem)
        tempSystem = result.fetchall()

        
        erpData = {}
        erpData['SystemName'] = message['SystemName']
        erpData['WorkflowId'] = message['WorkflowId']
        erpData['OperationName'] = message['OperationName']
        erpData['TaskId'] = message['TaskId']
        
        x = 0
        for sysItem in tempSystem:

            sysName = sysItem[x]
            Lambda_logger.info("Currently processing for : %s", sysName)

            qPlant = 'SELECT plant from ' + MDM_MM_SCHEMA + '.' + MDM_MM_PLANT + ' where material_number = ' + "'" + tempMaterial + "'" + ' and source = ' + "'" + sysName + "'"
            result = connection.execute(qPlant)
            tempPlant = result.fetchall()
            
            qSalesOrg = 'SELECT sales_org from ' + MDM_MM_SCHEMA + '.' + MDM_MM_SALES + ' where material_number = ' + "'" + tempMaterial + "'" + ' and source = ' + "'" + sysName + "'"
            result = connection.execute(qSalesOrg)
            tempSalesOrg = result.fetchall()
            
            qDistChannel = 'SELECT distr_channel from ' + MDM_MM_SCHEMA + '.' + MDM_MM_SALES + ' where material_number = ' + "'" + tempMaterial + "'" + ' and source = ' + "'" + sysName + "'"
            result = connection.execute(qDistChannel)
            tempDistChannel = result.fetchall()

            print("List of Plants, SalesOrg and Dist Channel from Redshift for the material " + tempMaterial, tempPlant, tempSalesOrg, tempDistChannel)
        

            if not tempPlant:

                if sysName == "JDE" or sysName == "PTMN":
                    rsData = {}
                    i = 1
                    Lambda_logger.info('Forming message for %s', sysName)
                    rsData['WorkflowId'] = message['WorkflowId']
                    rsData['OperationName'] = message['OperationName']
                    rsData['TaskId'] = message['TaskId']
                    rsData['SystemName'] = message['SystemName'] # Default Global -- used for mapping

                    rsDataSet = {}
                    rsDataTemp = {}
                    rsDataTemp['SystemName'] = sysName
                    rsDataTemp['MdmNumber'] = data['MdmNumber']
                    rsDataTemp['MaterialNum'] = data['MaterialNum']
                    rsDataTemp['MaterialGroup'] = data['MaterialGroup']
                    rsDataTemp['PH1Desc'] = message['Data'][i-1]['PH1Desc']
                    rsDataTemp['PH2Desc'] = message['Data'][i-1]['PH2Desc']
                    rsDataTemp['PH3Desc'] = message['Data'][i-1]['PH3Desc']
                    rsDataTemp['PH4Desc'] = message['Data'][i-1]['PH4Desc']
                    rsDataTemp['PH5Desc'] = message['Data'][i-1]['PH5Desc']
                    rsDataTemp['PH6Desc'] = message['Data'][i-1]['PH6Desc']
                    rsDataTemp['PH7Desc'] = message['Data'][i-1]['PH7Desc']
                    rsDataTemp['LifecycleDesc'] = message['Data'][i-1]['LifecycleDesc']
                    rsDataTemp['LifecycleStatus'] = message['Data'][i-1]['LifecycleStatus']
                    rsDataTemp['GoldenRecordIndicator'] = message['Data'][i-1]['GoldenRecordIndicator']

                    rsDataSet = rsDataTemp
                    rsData['Data'] = rsDataSet
                    erpData = rsData.copy()
                    Lambda_logger.info("Message formed for redshift update :: %s", rsData)
                    response = ProcessMaterials(rsData, erpData, engine , material1_golden, material1, material1_plant, material1_sales, connection)
        
            else:

                for iPlant, iSales, iDist in zip_longest(tempPlant, tempSalesOrg, tempDistChannel, fillvalue = " "):
                                
                    Lambda_logger.info('Forming message for SAP.. Total no of Plants for the material %s : %s', tempMaterial, len(tempPlant))    
                    intPlant = 0
                    Lambda_logger.info('Current plant : %s', iPlant[intPlant])   
                    intPlant = intPlant + 1
                    erpDataSet = {}
                    erpDataSet['MdmNumber'] = data['MdmNumber']
                    erpDataSet['MaterialNum'] = data['MaterialNum']
                    erpDataSet['MaterialGroup'] = data['MaterialGroup']
                    erpDataSet['SystemName'] = sysName
                
                    if not iPlant[0] == " ":
                        erpDataSet['Plant'] = iPlant[0]
                        erpDataSet['ProfitCenter'] = data['ProfitCenter']
                    else:
                        erpDataSet['Plant'] = ""
                        erpDataSet['ProfitCenter'] = ""
                        
                    if not iSales[0] == " ":
                        erpDataSet['SalesOrg'] = iSales[0]
                        erpDataSet['DistributionChannel'] = iDist[0]
                        erpDataSet['MaterialPricingGroup'] = data['MaterialPricingGroup']
                    else:
                        erpDataSet['SalesOrg'] = ""
                        erpDataSet['DistributionChannel'] = ""
                        erpDataSet['MaterialPricingGroup'] = ""

                    # erpDataBlock.append(erpDataSet)
                
                    erpData['Data'] = erpDataSet
                    
                    
                    #Lambda_logger.info('Message to ERP for approval :: %s ', erpData)
                    #Lambda_logger.info('--------------------------------------------------------------------------------')

                    rsData = {}
                    rsData = erpData.copy()
                    #rsData['Data']['SystemName'] = message['Data'][i-1]['SystemName']
                    rsData['Data']['PH1Desc'] = message['Data'][i-1]['PH1Desc']
                    rsData['Data']['PH2Desc'] = message['Data'][i-1]['PH2Desc']
                    rsData['Data']['PH3Desc'] = message['Data'][i-1]['PH3Desc']
                    rsData['Data']['PH4Desc'] = message['Data'][i-1]['PH4Desc']
                    rsData['Data']['PH5Desc'] = message['Data'][i-1]['PH5Desc']
                    rsData['Data']['PH6Desc'] = message['Data'][i-1]['PH6Desc']
                    rsData['Data']['PH7Desc'] = message['Data'][i-1]['PH7Desc']
                    rsData['Data']['LifecycleDesc'] = message['Data'][i-1]['LifecycleDesc']
                    rsData['Data']['LifecycleStatus'] = message['Data'][i-1]['LifecycleStatus']
                    rsData['Data']['GoldenRecordIndicator'] = message['Data'][i-1]['GoldenRecordIndicator']
                    Lambda_logger.info("Message formed for redshift update :: %s", rsData)
                    response = ProcessMaterials(rsData, erpData, engine , material1_golden, material1, material1_plant, material1_sales, connection)
                
                i = i + 1
        x = x + 1
        
    return response

def ProcessMaterials(rsData, material, engine, material1_golden, material1, material1_plant, material1_sales, connection):
    # Setup basic Workflow Details
    wfData = dict()
    wfData['WorkflowId'] = material['WorkflowId']
    wfData['OperationName'] = material['OperationName']
    wfData['TaskId'] = material['TaskId']
    wfData['SystemName'] = material['Data']['SystemName']
    wfData['MdmNumber'] = material['Data']['MdmNumber']
    wfData['MaterialNum'] = material['Data']['MaterialNum']

    '''
    # This code is commented to skip ERP stage temporarily

    Lambda_logger.info('Converting Business Friendly Fields to SAP X Fields')
    material['FieldX'] = "False" 
    erpFields = MapJsonFields(material, erp)
    erpFields = json.dumps(erpFields)

    Lambda_logger.info('Sending the converted JSON to ERP for approval')
    erpRes = UpdateErpMaterial(erpFields)
    Lambda_logger.info('ERP Response %s ', erpRes)
    
    '''


    #if erpRes['STATUS_CODE'] == 200:
    Lambda_logger.info('-------------------------------Skipping ERP---------------------------------------')
    Lambda_logger.info('Converting Business Friendly Fields to RedShift Fields')
    erpData = rsData.copy()
    
    # erpData['Data']['SystemName'] = rsData['SystemName']
    # Lambda_logger.info('Fields for Redshift conversion :: %s', erpData)
    mdmData = MapJsonFields(erpData, mdmdw)
    #Lambda_logger.info('RedShift Input mdmdata :: %s', mdmData)
    mdmData = json.loads(mdmData)

    # Update the MDM material data 
    UpdateMDMMaterial(mdmData, engine, material1_golden, material1, material1_plant, material1_sales, connection)
    dwresp={'STATUS_CODE':200}
    
    # Check if the MDM material is updated
    if dwresp['STATUS_CODE'] == 200:
        Lambda_logger.info('Updated MDM Material')
        wfData['IsSuccess'] = "TRUE"
        response.update({'STATUS_CODE':200})
        response.update({'STATUS_MSG':'Udpated MDM Material'})
    else:
        Lambda_logger.info('Unable to Update MDM Material')
        wfData['IsSuccess'] = "FALSE"
        response.update({'STATUS_CODE':-1})
        response.update({'STATUS_MSG':'Unable to Udpate MDM Material'})
    # else:
    #     Lambda_logger.info('Unable to Update ERP Material \n %s', erpRes['STATUS_MSG'])
    #     wfData['IsSuccess'] = 'FALSE'
    #     response.update({'STATUS_CODE':erpRes['STATUS_CODE']})
    #     response.update({'STATUS_MSG':erpRes['STATUS_MSG']})

    Lambda_logger.info('Success : Update global material - Process Ended')
    
    # Publish the WF message
    print ('Workflow Message : ', wfData)
    wfResponse = publishWF(json.dumps(wfData))
    print ('Response to Process : ', wfResponse)
    return response

def MapJsonFields(businessFields, convType):
    MDM_FIELD_MAPPING = os.environ['FN_FIELD_MAP']
    businessFields['Flag'] = convType
    # Lambda_logger.info('Data input for mapping :: %s', businessFields)
    response = lambda_client.invoke(
        FunctionName=MDM_FIELD_MAPPING,
        InvocationType='RequestResponse',
        Payload=json.dumps(businessFields)
        )  
    record = response['Payload'].read().decode('utf8').replace("'", '"')
    Lambda_logger.info('--------------------------------------------------------------------------------')
    Lambda_logger.info('Response from Mapping fields :: %s', record)
    return json.loads(record)

def UpdateMDMMaterial(mdmData, engine, material1_global, material1, material1_plant, material1_sales,connection):
    # Read environment variables 
    mand_keys = mdmData[MDM_MM_GOLDEN]['keys']
    # print('--------------------------------------------------------------------------------')
    print("Mandatory Keys :", mand_keys)
    Lambda_logger.info("Data to be pushed into Redshift tables :: %s", mdmData)
    #mdmData={'mdm_material': {'keys': {'source': 'APOLLO', 'mdm_material_id': 'MDMMM551000012', 'material_number': '000000000000001332'}, 'values': {}},'mdm_material_sales': {'keys': {'sales_org': '0120', 'distr_channel': '10'}, 'values': {'material_pricing_grp': 'RT', 'prod_hierarchy': '1505020'}},'mdm_material_plant': {'keys': {'plant': '0321'}, 'values': {'warehouse_no': '', 'storage_loc': '', 'storage_type': '', 'profit_center': '1005'}}}
    Lambda_logger.info('--------------------------------------------------------------------------------')
    Lambda_logger.info("Table Keys :: %s",mdmData.keys())
    upquery = ""
    
    for table in mdmData.keys():
        
        keys = mdmData[table]['keys']
        # keys.update(mand_keys)
        print("Updated Keys :", keys)
        values =  mdmData[table]['values']
        if(values!={}):
            Lambda_logger.info('Table in Progress :: %s', table)

            if table == MDM_MM_MATERIAL: 
                Lambda_logger.info('Updating Material Table')
                upquery = update(material1).where(
                    and_(
                        material1.c.source == mand_keys['source_system'],
                        material1.c.globalmdm == mand_keys['globalmdm'],
                        material1.c.material_number == mand_keys['material_number']
                        # material1_plant.c.material_group == keys['material_group']
                    )
                )
            elif table == MDM_MM_PLANT:
                if keys['plant'] != "":  
                    Lambda_logger.info('Updating Material Plant Table')            
                    upquery = update(material1_plant).where(
                        and_(
                            material1_plant.c.source == mand_keys['source_system'],
                            material1_plant.c.globalmdm == mand_keys['globalmdm'],
                            material1_plant.c.material_number == mand_keys['material_number'],
                            material1_plant.c.plant == keys['plant']
                            # material1_plant.c.profit_center == keys['profit_center']
                        )
                    )
            elif table == MDM_MM_SALES:
                if keys['sales_org'] != "":
                    Lambda_logger.info('Updating Material Sales Table')
                    upquery = update(material1_sales).where(
                        and_(
                            material1_sales.c.source == mand_keys['source_system'],
                            material1_sales.c.globalmdm == mand_keys['globalmdm'],
                            material1_sales.c.material_number == mand_keys['material_number'],
                            material1_sales.c.distr_channel==keys['distr_channel'],
                            material1_sales.c.sales_org == keys['sales_org']
                            # material1_sales.c.material_pricing_grp == keys['material_pricing_grp']              
                        )
                    )
            elif table == MDM_MM_GOLDEN:
                Lambda_logger.info('Updating Material Golden Table')
                upquery = update(material1_global).where(
                    and_(
                        material1_global.c.source_system == mand_keys['source_system'],
                        material1_global.c.globalmdm == mand_keys['globalmdm'],
                        material1_global.c.material_number == mand_keys['material_number']
                    )
                )
            try: 
                if upquery != "":
                    upquery = upquery.values(**values)
                    _upres = connection.execute(upquery)
                    uptable = upquery.table
                    print("The update query is  is", upquery)
                    #print(type(upres))
                    Lambda_logger.info('MDM Record Successfully Updated for the table : %s', uptable)
                    #connection.close()
                    response.update({'STATUS_CODE': 200})
                    response.update({'STATUS_MSG': 'Updated MDM Record'})    
                    #Lambda_loggsponse
                else:
                    Lambda_logger.info('Table : %s was skipped due to null values', uptable)

            except SQLAlchemyError as e:
                '''response.update({'STATUS_CODE': -1})
                response.update({'STATUS_MSG': 'Unable to Update Record'})'''
                Lambda_logger.info('Unable to Update Record :: %s', e)
                #connection.close()
                #return response
                #response1=response
        #return response1

        
    
def UpdateErpMaterial(data):
    Lambda_logger.info('Stage:ERP - Starting the process "Update Material Global"')
    
    # Assign environment variables 
    secret_name = os.environ['PIPO_SECRET']
    region_name = os.environ['Region']

    # Get the RedShift related secret keys from AWS Secret Manager
    job_secrets = get_secret(secret_name, region_name)
    secrets = json.loads(job_secrets)
    if secrets: 
        PIPO_USER = secrets['username']
        PIPO_PWD = secrets['password']
        PIPO_AUTH = secrets["auth"]
    else: 
        return {'STATUS_CODE':-1, 'STATUS_MSG':'Unable to Get Secrets'}

    PIPO_CUST_UPDT = os.environ['PIPO_ENDPOINT_UPDATE']
    # Setup Header and trgger PIPO endpoint
    headers = {"Authorization":PIPO_AUTH}
    payload = json.loads(data)
    response = requests.post(PIPO_CUST_UPDT,data=payload, timeout=30, auth=(PIPO_USER,PIPO_PWD), headers=headers)
    #print("response is",response)
    res = response.json()
    return res['E_RETURN']

def publishES(data):
    # This method will publish MDM record to Elastic Search SNS Topic
    MDM_ES_SNS = secrets['PUBLISH_ES_SNS']
    response = lambda_client.invoke(
        FunctionName=MDM_ES_SNS,
        InvocationType='RequestResponse',
        Payload=json.dumps(data)
        )
    record = response['Payload'].read().decode('utf8').replace("'", '"')
    return json.loads(record)    
    
def publishWF(data):
    # This method will publish upstream response
    MDM_WF_SNS = os.environ['PUBLISH_WF_SNS']
    response = lambda_client.invoke(
        FunctionName=MDM_WF_SNS,
        InvocationType='RequestResponse',
        Payload=json.dumps(data)
        )
    record = response['Payload'].read().decode('utf8').replace("'", '"')
    return json.loads(record)

def get_secret(secret_name, region_name):
    
    # Create a Secrets Manager client
    Lambda_logger.info('Reading Secrets from AWS')    
    client = boto3.client(
        service_name='secretsmanager',
        region_name=region_name
    )

    try:
        get_secret_value_response = client.get_secret_value(
            SecretId=secret_name
        )
    except ClientError as e:
        if e.response['Error']['Code'] == 'DecryptionFailureException':
            # Secrets Manager can't decrypt the protected secret text using the provided KMS key.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'InternalServiceErrorException':
            # An error occurred on the server side.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'InvalidParameterException':
            # You provided an invalid value for a parameter.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'InvalidRequestException':
            # You provided a parameter value that is not valid for the current state of the resource.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'ResourceNotFoundException':
            # We can't find the resource that you asked for.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
    else:
        # Decrypts secret using the associated KMS CMK.
        # Depending on whether the secret is a string or binary, one of these fields will be populated.
        if 'SecretString' in get_secret_value_response:
            secret = get_secret_value_response['SecretString']
            Lambda_logger.info('Secret has been retrieved')
            Lambda_logger.info('Secrets :: %s', secret_name)
            return secret
        else:
            decoded_binary_secret = base64.b64decode(get_secret_value_response['SecretBinary'])
            return decoded_binary_secret