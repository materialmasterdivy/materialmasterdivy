###################################################################
#
#  Lambda Name : MdmDataIntegrationMM
#  Purpose     : This is driver Lambda function for Material Master
#
#  Featues:
#  [1] - CreateMaterial
#  [1] - CreateFertMaterial
#  [1] - UdpateMaterial
#  [1] - EOLMaterial
#  [1] - ReserveMaterial
#
#  Stages:
#  [1] - Process the message through event trigger
#  [1] - Identify the Lambda to be triggered via the field 'Operation Name'
#  [1] - Complete the process
#  
#  Input  : event - <SQS Trigger>
#  Output : Publish to SNS topics below
#            a. WF SNS
#            b. ES SNS
#
#  Change Log:
#  05/05/2020 - Initial version
#  11/09/2020 - Handled EOL and updated the SQS details
###################################################################