import json
import boto3
import logging
import os 

# Set up Logging feature for CloudWatch Logs
Lambda_logger = logging.getLogger()
Lambda_logger.setLevel(logging.INFO)

# Read Environment Variables 
sqs_queue_url = os.environ['SQS_QUEUE_URL']

# MM_DI_CREATE_MAT = os.environ['MM_CREATE_MAT']
# MM_DI_CREATEFERT_MAT = os.environ['FN_CREATE_FERT']
# MM_DI_RESERVE_MAT = os.environ['FN_RES_MAT']
MM_DI_UPDATE_MAT = os.environ['FN_UPDT_MAT']
MM_DI_EOL_MAT = os.environ['FN_EOL_MAT']
MM_DI_UPDATE_GLOBAL = os.environ['FN_UPDATE_GLOBAL_MAT']

# Dummy Initializations 
MM_DI_CREATE_MAT = ""
MM_DI_CREATEFERT_MAT = ""
MM_DI_RESERVE_MAT = ""

# Set up Boto3 clients
lambda_client = boto3.client('lambda')
sqs = boto3.client('sqs')

# Response Template
response = {}

def lambda_handler(event, context):
    Lambda_logger.info('Starting Material Master Data Integration Process')    
    
    if not event:
        return {"STATUS_CODE":200}
        
    # Consume JSON from the event
    Lambda_logger.info('Received Event is :: %s', event)      
    for e in event['Records']:
        ReceiptHandle = e['receiptHandle']    
        #Lambda_logger.info('ReceiptHandle:: %s', ReceiptHandle)      

        message = {}
        
        strMsg = e['body']
        #Lambda_logger.info('Dev strMsg :: %s', strMsg)
        
        data = json.loads(strMsg)
       # Lambda_logger.info('Dev data :: %s', data)
        
        message['Message'] = json.loads(data['Message'])
        Operation = message['Message']['OperationName']
        
        Lambda_logger.info('The operation received is %s', Operation)
        Lambda_logger.info('Message %s', message)

        if Operation =='CreateMaterial':
            response = invokeFunction(MM_DI_CREATE_MAT, message)
        elif Operation == 'UpdateMaterial':
            response = invokeFunction(MM_DI_UPDATE_MAT, message)
        elif Operation == 'EndOfLifeMaterial':
            response = invokeFunction(MM_DI_EOL_MAT, message)
        elif Operation == 'EndOfLifeMaterial':
            response = invokeFunction(MM_DI_EOL_MAT, message)
        elif Operation == 'CreateMaterialFert':
            response = invokeFunction(MM_DI_CREATEFERT_MAT, message)
        elif Operation == 'UpdateGlobal':
            response = invokeFunction(MM_DI_UPDATE_GLOBAL, message)     
        else:
            print ('Unrecognized OperationName ',Operation)
            response['STATUS_CODE'] = -1
            response['STATUS_MSG'] = 'Unrecognized Operation Name'

        # Delete the SQS message on success
        if response['STATUS_CODE'] == 200:
            # Delete received message from queue
            Lambda_logger.info('Deleting SQS Message from the Queue')
            if 'ReceiptHandle' in event:
                sqs.delete_message(
                    QueueUrl=sqs_queue_url,
                    ReceiptHandle=ReceiptHandle
                )
        else:
            # Move the Unprocessed message to Dead Letter Queue
            Lambda_logger.info("MDM Operation %s", response) 

    Lambda_logger.info('MM Data Integration Process is completed for - %s', Operation)

def invokeFunction(lambdaArn, data):
    response = lambda_client.invoke(
        FunctionName=lambdaArn,
        InvocationType='RequestResponse',
        Payload=json.dumps(data)
        )
    record = response['Payload'].read().decode('utf8').replace("'", '"')
    return json.loads(record)