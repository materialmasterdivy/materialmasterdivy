import os
import json
import boto3
import requests 
import logging 

sns = boto3.client('sns')
TopicArn = os.environ['MDM_SNS_TOPIC_ARN']

# Set up Logging feature for CloudWatch Logs
Lambda_logger = logging.getLogger()
Lambda_logger.setLevel(logging.INFO)
    
def lambda_handler(event, context):
    Lambda_logger.info('Starting Workflow Upstream Process')
    # Check if its a health check
    if not event:
        return {"STATUS_CODE":200}

    response = sendWorkFlowRecord(event)
    ResMeta = response['ResponseMetadata']
    print (ResMeta['HTTPStatusCode'])
    if ResMeta['HTTPStatusCode'] == 200:
        print ('Published Message to topic')
        return {"status_code":ResMeta['HTTPStatusCode'], "Message":"Message Sent"}
    else:
        print ('Unable to Publish Message to topic')
        return {"status_code":ResMeta['HTTPStatusCode'], "Message":"Message Not Sent"}

def sendWorkFlowRecord (MdmMaterial):
    Lambda_logger.info('Publishing messages to %s', TopicArn)
    Lambda_logger.info('Workflow Message Sent %s', MdmMaterial)
    response = sns.publish(
        TargetArn=TopicArn, 
        Message=json.dumps(MdmMaterial)) 
    Lambda_logger.info('Workflow Message Sent %s', response)                      
    return response