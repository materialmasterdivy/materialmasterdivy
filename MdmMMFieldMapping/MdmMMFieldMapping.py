import json
import logging 
import os 
import pymysql
import sqlalchemy as db 
import traceback 
import boto3
import base64
from sqlalchemy import Table, Column, String, Integer, create_engine
from sqlalchemy import  select, insert, text, Boolean, TIMESTAMP, and_
from sqlalchemy import MetaData
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.orm.exc import NoResultFound
from botocore.exceptions import ClientError


# Set up Logging feature for CloudWatch Logs
Lambda_logger = logging.getLogger()
Lambda_logger.setLevel(logging.INFO)

def lambda_handler(event, context):

    # Check if its a health check
    if not event:
        return {"STATUS_CODE":200}

    # Assign environment variables
    MDM_MYSQL_CNCTN = os.environ['MYSQL_CNCTN_STRING']
    MDM_MYSQL_SRC_TBL = os.environ['MYSQL_CM_SRC_TABLE']
    MDM_MYSQL_SYS_TBL = os.environ['MYSQL_CM_SYS_TABLE']  
    # secret_name = os.environ['MYSQL_SECRET']
    # region_name = os.environ['Region']
    response = {} 

    system = event['SystemName']
    operation = event['OperationName']
    fieldX = False
    if 'FieldX' in event: fieldX = event['FieldX']

    print("Incoming System : ", system)
    print("Incoming Operation :", operation)

    print ('fieldX : ', fieldX)
    try:
        engine = db.create_engine(MDM_MYSQL_CNCTN)
        connection = engine.connect()
        md = db.MetaData()
        Lambda_logger.info('MySQL Connected successfully')
    except SQLAlchemyError as e:
        response.update({'STATUS_CODE': -1})
        response.update({' STATUS_MSG':'MySQL Connection Error'})
        Lambda_logger.info('MySQL Connection Issue :: %s', e)
        return response

    # Create Table objects 
    Source = db.Table(MDM_MYSQL_SRC_TBL,md, autoload=True, autoload_with=engine)
    System = db.Table(MDM_MYSQL_SYS_TBL,md, autoload=True, autoload_with=engine)

    print("Source", Source)
    print("System", System)

    join_sql = Source.join(System, System.c.Id == Source.c.SystemId)
    select_st = select([Source.c.SourceFieldName, Source.c.BusinessFieldName, Source.c.RedshiftFieldName, Source.c.SourceKeyField, Source.c.RedshiftKeyField, Source.c.RedShiftTableName]) \
        .where(and_(
            System.c.Value == system,
            Source.c.OperationName == operation)
            ).select_from(join_sql)

    # print("Formed Query : ", select_st)
    try:
        rp2 = connection.execute(select_st)
        res = rp2.fetchall()
        # print("Res : ", res)
        # Lambda_logger.info('Formed query for mapping :: %s', select_st)
        # Lambda_logger.info('DB Retreival - Successful')
    except SQLAlchemyError as e:
        response.update({'STATUS_CODE': -1})
        response.update({'STATUS_MSG':'Unable to Fetch Source Field Mapping'})
        Lambda_logger.info('Unable to Fetch Source Field Mapping')
        connection.close ()        
        return response

    result = []
    for a, b, c, d, e, f in res:
        dict_val = {'SourceFieldName': a, 'BusinessFieldName':b, 'RedshiftFieldName':c, 'SourceKeyField': d, 'RedshiftKeyField': e, 'RedShiftTableName': f}
        result.append(dict_val)
    # print("Result : ", result)  
    msg = {}
    try:     
        if event['Flag'] == 1:
            Lambda_logger.info('Incoming request :: Business to ERP mapping (flag 1)')
            keyList = event["Data"].keys()
            print(keyList)
            # print("Message keys", msg.keys())
            result = [row for row in result if row['BusinessFieldName'] in keyList]
            for row in result:
                # print("Row item : ", row)
                SourceFieldName = row['SourceFieldName']
                BusinessFieldName = row['BusinessFieldName']
                SourceKeyField = row['SourceKeyField']
                
                if SourceKeyField == 'ROOT':
                    msg.update({SourceFieldName: event["Data"][BusinessFieldName]})
                elif SourceKeyField == 'DNU':
                    continue
                else:
                    if SourceKeyField not in msg.keys():
                        msg.update({SourceKeyField: {}})

                    msg[SourceKeyField].update({SourceFieldName: event["Data"][BusinessFieldName]})

                    if fieldX:
                        msg[SourceKeyField].update({f'{SourceFieldName}X': 'X'})

        elif event['Flag'] == 2:
            Lambda_logger.info('Incoming request :: (flag 2)')
            keyList = event["Data"].keys()
            print ('Key : ', keyList)
            
            result = [row for row in result if row['SourceFieldName'] in keyList]
            for row in result:
                SourceFieldName = row['SourceFieldName']
                BusinessFieldName = row['BusinessFieldName']
                
                msg.update({BusinessFieldName: event["Data"][SourceFieldName]})
                
        elif event['Flag'] == 3:
            Lambda_logger.info('Incoming request :: Business to RedShift mapping flag(3)')
            print(type(event['Data']))
            print(event['Data'])
            print("--------------------------------------------------------------------")
            keyList = event["Data"].keys()
            print ('KeyList : ', keyList)
            result = [row for row in result if row['BusinessFieldName'] in keyList]
            print("--------------------------------------------------------------------")
            print ('Result : ', result)
            
            for row in result:
                RedshiftFieldName = row['RedshiftFieldName']
                BusinessFieldName = row['BusinessFieldName']
                RedshiftKeyField = row['RedshiftKeyField']
                RedShiftTableName = row['RedShiftTableName']
                
                if not RedShiftTableName in msg.keys():
                    msg.update({RedShiftTableName: {'keys':{}, 'values':{}}})
                    
                if RedshiftKeyField:
                    msg[RedShiftTableName]['keys'].update({RedshiftFieldName: event["Data"][BusinessFieldName]})
                else:
                    msg[RedShiftTableName]['values'].update({RedshiftFieldName: event["Data"][BusinessFieldName]})
        
        elif event['Flag'] == 4:
            Lambda_logger.info('Incoming request :: flag 4')
            keyList = event["Data"].keys()
            
            result = [row for row in result if row['RedshiftFieldName'] in keyList]
            for row in result:
                RedshiftFieldName = row['RedshiftFieldName']
                BusinessFieldName = row['BusinessFieldName']                
                msg.update({BusinessFieldName: event["Data"][RedshiftFieldName]})
        
    except:
            json.dumps({
            "STATUS_CODE":  -1,
            "STATUS_MSG": 'Unable to Convert JSON'
        })  
    event.pop("Data")

    event.update({"Data":msg})
    Lambda_logger.info('Closing MySQL Connection')
    return  json.dumps(event["Data"])

def get_secret(secret_name, region_name):
    
    # Create a Secrets Manager client
    Lambda_logger.info('Reading Secrets from AWS')    
    client = boto3.client(
        service_name='secretsmanager',
        region_name=region_name
    )

    try:
        get_secret_value_response = client.get_secret_value(
            SecretId=secret_name
        )
    except ClientError as e:
        if e.response['Error']['Code'] == 'DecryptionFailureException':
            # Secrets Manager can't decrypt the protected secret text using the provided KMS key.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'InternalServiceErrorException':
            # An error occurred on the server side.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'InvalidParameterException':
            # You provided an invalid value for a parameter.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'InvalidRequestException':
            # You provided a parameter value that is not valid for the current state of the resource.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'ResourceNotFoundException':
            # We can't find the resource that you asked for.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
    else:
        # Decrypts secret using the associated KMS CMK.
        # Depending on whether the secret is a string or binary, one of these fields will be populated.
        if 'SecretString' in get_secret_value_response:
            secret = get_secret_value_response['SecretString']
            Lambda_logger.info('Secret has been retrieved')
            return secret
        else:
            decoded_binary_secret = base64.b64decode(get_secret_value_response['SecretBinary'])
            return decoded_binary_secret